[![Flatris](flatris.png)](https://flatris.space/)

[![Build Status](https://travis-ci.org/skidding/flatris.svg?branch=master)](https://travis-ci.org/skidding/flatris)

> **Work in progress:** Flatris has been recently redesigned from the ground up and turned into a multiplayer game with both UI and server components. This has been an interesting journey and I plan to document the architecture in depth. **[Stay tuned](https://twitter.com/skidding)**.

[![Flatris](flatris.gif)](https://flatris.space/)

> **Contribution disclaimer:** Flatris is a web game with an opinionated feature set and architectural design. It doesn't have a roadmap. While I'm generally open to ideas, I would advise against submitting unannounced PRs with new or modified functionality. That said, **bug reports and fixes are most appreciated.**

Thanks [@paulgergely](https://twitter.com/paulgergely) for the initial flat design!

Also see [elm-flatris](https://github.com/w0rm/elm-flatris).


## Setup and running

```
yarn install
yarn test
yarn build
yarn start
```

Go to http://localhost:3000


## Release

This is completed pipeline, that makes your Flatris app available world-wide using Yandex.Cloud Managed Kuberneres.
You need to specify your Yandex.Cloud settings as CI/CD variables: 
 - YC_CLOUD_ID - Yandex CLoud ID
 - YC_FOLDER_ID - Yandex Folder ID
 - YC_PROFILE_NAME - Yandex Profile Name
 - YANDEX_KEY (as file) - Service Account Key (JSON)

Note: **release** stage runs only after commiting with tag. This tag used also in docker image as well as in final deployment yml-file.
Follow **release** stage to get URL for accessing hosted app. Current - http://84.201.138.61:3000

Enjoy!

## ToDo

1. Add HTTP(S) proxy with public domain-name and LetsEncrypt SSL-certificates.
2. Separate docker image with pre-installed **Yandex CLoud CLI** utils and **kubectl** for speedup release stage.
3. helm or similar templating tool to make yml-files more readable and flexible (configure number of replicas etc.)
