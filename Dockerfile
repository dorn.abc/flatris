FROM node:12.16.1-stretch-slim

WORKDIR /app

COPY package.json yarn.lock ./

# Speedup docker builds
RUN yarn install

COPY . /app

RUN yarn build
#RUN yarn test

EXPOSE 3000
CMD ["yarn", "start"]
